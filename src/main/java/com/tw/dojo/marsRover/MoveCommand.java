package com.tw.dojo.marsRover;

public class MoveCommand implements ICommand{
    @Override
    public Position run(Position position) {
        return new Position(move(position.getCoordinate(), position.getDirection()), position.getDirection());

    }
    private Coordinate move(Coordinate coordinate, Direction direction) {
        return new Coordinate(coordinate.getX() + direction.x, coordinate.getY() + direction.y);
    }
}
