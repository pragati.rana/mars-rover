package com.tw.dojo.marsRover;

import openj9.internal.tools.attach.target.Command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MarsRover {

    public String run(String input) {
        String out = "";

        String[] lines = input.split("\n");

        int numberOfRovers = (lines.length - 1) / 2;

        for (int i = 0; i < numberOfRovers; i++) {
            int positionLineIndex = i * 2 + 1;
            int commandLineIndex = positionLineIndex + 1;

            Coordinate coordinate = getInitialPosition(lines[positionLineIndex]);

            Direction direction = getInitialDirection(lines[positionLineIndex]);

            List<ICommand> commands = getInitialCommand(lines, commandLineIndex);

            Position position = new Position(coordinate, direction);

            for (ICommand command : commands) {
                position = command.run(position);
            }

            out += position.toString();
        }

        return out;
    }


    private List<ICommand> getInitialCommand(String[] lines, int commandLineIndex) {
        String[] commandArray = lines[commandLineIndex].split("(?!^)");

        List<ICommand> commandsList = new ArrayList<>();

        List<String> validCommands = Arrays.asList("M", "R", "L");
        for (String command : commandArray) {
            if (!validCommands.contains(command)) {
                throw new IllegalArgumentException("Invalid command sequence: " + lines[commandLineIndex]);
            }

            commandsList.add((Commands.valueOf(command)).getCommand());
        }
        return commandsList;
    }

    private Direction getInitialDirection(String lines) {
        String direction;

        try {
            direction = lines.split(" ")[2];
            if (!Arrays.asList("N", "E", "S", "W").contains(direction)) {
                throw new IllegalArgumentException();
            }
        } catch (RuntimeException e) {
            throw new IllegalArgumentException("Could not parse direction from: " + lines);
        }
        return Direction.valueOf(direction);
    }

    private Coordinate getInitialPosition(String lines) {
        int xWidth, yWidth;

        try {
            String[] split = lines.split(" ");

            xWidth = Integer.parseInt(split[0]);
            yWidth = Integer.parseInt(split[1]);
        } catch (RuntimeException e) {
            throw new IllegalArgumentException("Could not parse position from: " + lines);
        }

        return new Coordinate(xWidth, yWidth);
    }

}
