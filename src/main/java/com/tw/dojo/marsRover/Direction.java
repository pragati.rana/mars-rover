package com.tw.dojo.marsRover;

import java.util.Arrays;
import java.util.List;

public enum Direction {
    N(0, 1),
    E(1, 0),
    S(0, -1),
    W(-1, 0);

    int x;
    int y;

    Direction(int x, int y) {
        this.x = x;
        this.y = y;
    }

    Direction turnLeft() {
        List<Direction> all = Arrays.asList(values());
        return all.get((all.indexOf(this) + 3) % all.size());
    }

    Direction turnRight() {
        List<Direction> all = Arrays.asList(values());
        return all.get((all.indexOf(this) + 1) % all.size());
    }
}
