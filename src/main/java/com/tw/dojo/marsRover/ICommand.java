package com.tw.dojo.marsRover;

public interface ICommand {
    public Position run(Position position);
}
