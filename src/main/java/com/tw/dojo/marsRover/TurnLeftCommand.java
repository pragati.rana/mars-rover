package com.tw.dojo.marsRover;

public class TurnLeftCommand implements ICommand{
    @Override
    public Position run(Position position) {
        return new Position(position.getCoordinate(), position.getDirection().turnLeft());
    }
}
