package com.tw.dojo.marsRover;

public enum Commands {
    M(new MoveCommand()),
    R(new TurnRightCommand()),
    L(new TurnLeftCommand());


    private final ICommand command;

    Commands(ICommand command) {
        this.command = command;
    }

    public ICommand getCommand() {
        return command;
    }

}
